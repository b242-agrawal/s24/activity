let getCube = function cube(number){
	return number**3
}
console.log(`The cube of 2 is ${getCube(2)}`);

let address = [258, "Washington Ave NW", "California", 90011];
let [number, city, state, pincode] = address;
console.log(`I live at ${number} ${city}, ${state} ${pincode}`);

let animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);

let array = [1,2,3,4,5];
array.forEach((number) => console.log(number));
let sum = array.reduce((x,y) => x+y);
console.log(sum)

function Dog(name, age, breed) {
	this.name = name;
	this.age = age;
	this.breed =  breed;
}
let frankie = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(frankie);